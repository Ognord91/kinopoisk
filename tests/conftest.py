import pytest

from fake_useragent import UserAgent
from selenium import webdriver
from selenium.common import NoSuchElementException
from selenium.webdriver.chrome.webdriver import WebDriver
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.chrome.service import Service as ChromiumService

from pages.captcha_page.captcha_page_locators import Captcha
from pages.data_structs import KinoTopResources
from pages.top_page.top_page import TopPage


@pytest.fixture(scope="session")
def fake_user() -> str:
    """
    Генерирует фейковый юзер агент, необходим для
    упрощения прохождения капчи.
    :return: UserAgent
    """
    ua = UserAgent(platforms=["pc"])
    user_agent: str = ua.random
    return user_agent

@pytest.fixture(scope="session")
def browser(fake_user: str) -> TopPage:
    """
    Фикстура загружает веб драйвер, открывает страницу с топ 250 фильмов.
    По необходимости обходит капчу, если она есть.
    :param fake_user: UserAgent
    :yield: TopPage
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("user-agent=" + fake_user)

    driver: WebDriver = webdriver.Chrome(
        service=ChromiumService(ChromeDriverManager().install()), options=chrome_options
    )
    driver.maximize_window()
    driver.get(KinoTopResources.URL)

    try:
        driver.find_element(*Captcha.CAPTCHA).click()
    except NoSuchElementException:
        pass

    browser = TopPage(driver, KinoTopResources.URL)
    yield browser
    driver.quit()
