import allure
from selenium.webdriver.remote.webelement import WebElement

from blocks_with_elements.filter.filter_locators import FilterBlockLocators
from blocks_with_elements.filter.filter_static_texts import FilterBlockTexts
from pages.top_page.top_page import TopPage


@allure.story("1_1 Test_present_all_filter_elements")
def test_present_all_filter_elements(browser: TopPage):
    """
    На странице представлены все элементы фильтра
    """
    with allure.step("Step 1: Present filter block"):
        filter_block: list[WebElement] = browser.get_elements(FilterBlockLocators.BLOCK_FILTERS)
        browser.check_quantity_elements(filter_block, quantity_elements=1)

    with allure.step("Step 2: Present filters buttons"):
        filter_buttons: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON)
        browser.check_quantity_elements(filter_buttons, quantity_elements=len(FilterBlockTexts.FILTER_BUTTON_TEXTS))

    with allure.step("Step 3: Present filters selectors"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)
        browser.check_quantity_elements(
            filter_selectors,
            quantity_elements=len(FilterBlockTexts.FILTER_SELECTORS_TEXTS)
        )


@allure.story("1_2 Ttest_texts_all_filter_elements")
def test_texts_all_filter_elements(browser: TopPage):
    """
    У элементов фильтра есть ожидаемые текста
    """
    with allure.step("Step 1: Check text filter buttons"):
        filter_buttons: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON)
        browser.check_texts_elements(filter_buttons, FilterBlockTexts.FILTER_BUTTON_TEXTS)

    with allure.step("Step 2: Check text filter selectors"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)
        browser.check_texts_elements(filter_selectors, FilterBlockTexts.FILTER_SELECTORS_TEXTS)


@allure.story("1_3 Ttest_check_present_disable_button_filter")
def test_check_present_disable_button_filter(browser: TopPage):
    """
    Проверка заблокированных кнопок фильтра
    """
    with allure.step("Step 1: Present filter disable buttons"):
        filter_disable_buttons: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON_DISABLE)
        browser.check_quantity_elements(
            filter_disable_buttons,
            quantity_elements=len(FilterBlockTexts.BUTTON_DISABLE_TEXTS)
        )

    with allure.step("Step 2: Check text filter disable buttons"):
        browser.check_texts_elements(filter_disable_buttons, FilterBlockTexts.BUTTON_DISABLE_TEXTS)

    with allure.step("Step 3: Check click filter disable buttons"):
        browser.click_disable_buttons(filter_disable_buttons)


@allure.story("1_4 Test_select_element_and_open_close_selector")
def test_activate_deactivate_button(browser: TopPage):
    """
    Тест активации-деактивации кнопки фильтра
    """
    with allure.step("Step 1: Present filter buttons"):
        filter_buttons: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON)

    with allure.step("Step 2: Random select not disabled button"):
        generate_random_values_button: tuple[int, str] = (
            browser.filtrate_random_generate_data(FilterBlockTexts.FILTER_BUTTON_TEXTS)
        )

    with allure.step("Step 3: Activate selected button"):
        browser.click_enable_button(filter_buttons[generate_random_values_button[0]])

    with allure.step("Step 4: Find all activated buttons"):
        filter_buttons_on: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON_ON)

    with allure.step("Step 5: Check that text of activated button corresponds to selected option"):
        browser.check_text_element(filter_buttons[generate_random_values_button[0]], filter_buttons_on[0].text)

    with allure.step("Step 6: Make sure that quantity of activated buttons corresponds to expected quantity"):
        browser.check_quantity_elements(filter_buttons_on, quantity_elements=1)

    with allure.step("Step 7: Deactivate selected button"):
        browser.click_enable_button(filter_buttons_on[0])

    with allure.step("Step 8: Find all activated buttons"):
        filter_buttons_on: list[WebElement] = browser.get_elements(
            FilterBlockLocators.FILTER_BUTTON_ON,
            element_exist=False
        )

    with allure.step("Step 9: Make sure that quantity of activated buttons corresponds to expected quantity"):
        browser.check_quantity_elements(filter_buttons_on, quantity_elements=0)


@allure.story("1_5 Test_check_selector_elements")
def test_check_selector_elements(browser: TopPage):
    """
    Проверка наличия элементов селектора
    """
    with allure.step("Step 1: Present filter selectors"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)

    with allure.step("Step 2: Check elements selector: COUNTRIES"):

        with allure.step("Step 2_1: Present elements selector: COUNTRIES"):

            browser.click_enable_button(filter_selectors[0])
            selector_countries_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)
            browser.check_quantity_elements(
                selector_countries_elements,
                quantity_elements=len(FilterBlockTexts.SELECTOR_ELEMENT_COUNTRIES_TEXT)
            )

        with allure.step("Step 2_2: Check texts elements selector: COUNTRIES"):
            browser.check_texts_elements(selector_countries_elements, FilterBlockTexts.SELECTOR_ELEMENT_COUNTRIES_TEXT)
            browser.click_enable_button(filter_selectors[0])

    with allure.step("Step 3: Check elements selector: GENRE"):

        with allure.step("Step 3_1: Present elements selector: GENRE"):
            browser.click_enable_button(filter_selectors[1])
            selector_genre_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)
            browser.check_quantity_elements(
                    selector_genre_elements, quantity_elements=len(FilterBlockTexts.SELECTOR_ELEMENT_GENRE_TEXTS)
                )

        with allure.step("Step 3_2: Check texts elements selector: GENRE"):
            browser.check_texts_elements(selector_genre_elements, FilterBlockTexts.SELECTOR_ELEMENT_GENRE_TEXTS)
            browser.click_enable_button(filter_selectors[1])

    with allure.step("Step 4: Check elements selector: AGES"):

        with allure.step("Step 4_1: Present elements selector: AGES"):
            browser.click_enable_button(filter_selectors[2])
            selector_ages_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)
            browser.check_quantity_elements(
                    selector_ages_elements, quantity_elements=len(FilterBlockTexts.SELECTOR_ELEMENT_AGES_TEXTS)
                )

        with allure.step("Step 4_2: Check texts elements selector: AGES"):
            browser.check_texts_elements(selector_ages_elements, FilterBlockTexts.SELECTOR_ELEMENT_AGES_TEXTS)
            browser.click_enable_button(filter_selectors[2])


@allure.story("1_6 Test_selector_element_is_not_selected")
def test_selector_element_is_not_selected(browser: TopPage):
    """
    Открытие-закрытие селектора не изменяет выбранный по умолчанию элемент
    """
    with allure.step("Step 1: Present_elements_selector"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)

    with allure.step("Step 2: Random select one of selectors to check"):
        generate_random_value: tuple = browser.get_random_value_and_index(
            FilterBlockTexts.FILTER_SELECTORS_TEXTS)

    with allure.step("Step 3: Open close the selected selector"):
        browser.click_enable_button(filter_selectors[generate_random_value[0]])
        browser.click_enable_button(filter_selectors[generate_random_value[0]])

    with allure.step("Step 4: Check default selected text has not changed"):
        browser.check_text_element(filter_selectors[generate_random_value[0]], generate_random_value[1])


@allure.story("1_7 Test_select_element_selector")
def test_select_element_selector(browser: TopPage):
    """
    Проверка выбора элементов селектора
    """
    with allure.step("Step 1: Present_elements_selector"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)

    with allure.step("Step 2: Check elements selector: COUNTRIES"):

        with allure.step("Step 2_1: Random select one of selectors to check"):
            generate_random_value_countries: tuple = browser.get_random_value_and_index(
                FilterBlockTexts.SELECTOR_ELEMENT_COUNTRIES_TEXT)

        with allure.step("Step 2_2: Open the selector, activate the selected item"):
            browser.click_enable_button(filter_selectors[0])
            selector_countries_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)

        with allure.step("Step 2_3: Check sure that text of selector element matches selected value"):
            browser.check_text_element(selector_countries_elements[generate_random_value_countries[0]],
                                       generate_random_value_countries[1])

        with allure.step("Step 2_4: Activate the selected item"):
            browser.click_enable_button(selector_countries_elements[generate_random_value_countries[0]])

    with allure.step("Step 3: Check elements selector: GENRE"):

        with allure.step("Step 3_1: Random select one of selectors to check"):
            generate_random_value_genre: tuple = browser.get_random_value_and_index(
                FilterBlockTexts.SELECTOR_ELEMENT_GENRE_TEXTS)

        with allure.step("Step 3_2: Open the selector, activate the selected item"):
            browser.click_enable_button(filter_selectors[1])
            selector_genre_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)

        with allure.step("Step 3_3: Check sure that text of selector element matches selected value"):
            browser.check_text_element(
                selector_genre_elements[generate_random_value_genre[0]],
                generate_random_value_genre[1]
            )

        with allure.step("Step 3_4: Activate the selected item"):
            browser.click_enable_button(selector_genre_elements[generate_random_value_genre[0]])

    with allure.step("Step 4: Check elements selector: AGES"):

        with allure.step("Step 4_1: Random select one of selectors to check"):
            generate_random_value_ages: tuple = browser.get_random_value_and_index(
                FilterBlockTexts.SELECTOR_ELEMENT_AGES_TEXTS)

        with allure.step("Step 4_2: Open the selector, activate the selected item"):
            browser.click_enable_button(filter_selectors[2])
            selector_ages_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)

        with allure.step("Step 4_3: Check sure that text of selector element matches selected value"):
            browser.check_text_element(
                selector_ages_elements[generate_random_value_ages[0]],
                generate_random_value_ages[1]
            )

        with allure.step("Step 4_4: Activate the selected item"):
            browser.click_enable_button(selector_ages_elements[generate_random_value_ages[0]])

    with allure.step("Step 5: Check that selected values are displayed in forms of selected selectors"):

        with allure.step("Step 5_1: Check selector: COUNTRIES"):
            browser.check_text_element(filter_selectors[0], generate_random_value_countries[1])

        with allure.step("Step 5_2: Check selector: GENRE"):
            browser.check_text_element(filter_selectors[1], generate_random_value_genre[1])

        with allure.step("Step 5_2: Check selector: AGES"):
            browser.check_text_element(filter_selectors[2], generate_random_value_ages[1])


@allure.story("1_8 Test_select_element_and_open_close_selector")
def test_select_element_and_open_close_selector(browser: TopPage):
    """
    Открытие, закрытие селектора не изменяет выбранное значение
    """
    with allure.step("Step 1: Present elements selector"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)

    with allure.step("Step 2: Random select one of selectors to check"):
        generate_random_value_countries: tuple = browser.get_random_value_and_index(
            FilterBlockTexts.SELECTOR_ELEMENT_COUNTRIES_TEXT)

    with allure.step("Step 3: Open the selector, activate the selected item"):
        browser.click_enable_button(filter_selectors[0])
        selector_countries_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)

    with allure.step("Step 4: Check sure that text of selector element matches selected value"):
        browser.check_text_element(selector_countries_elements[generate_random_value_countries[0]],
                                   generate_random_value_countries[1])

    with allure.step("Step 5: Activate the selected item"):
        browser.click_enable_button(selector_countries_elements[generate_random_value_countries[0]])

    with allure.step("Step 6: Open-Close selector"):
        browser.click_enable_button(filter_selectors[0])
        browser.click_enable_button(filter_selectors[0])

    with allure.step("Step 7: Check selected text has not changed"):
        browser.check_text_element(filter_selectors[0], generate_random_value_countries[1])


@allure.story("1_9 Test_selector_choice_disables_filter_buttons")
def test_selector_choice_disables_filter_buttons(browser: TopPage):
    """
    Активация СССР в селекторе, блокирует кнопку фильтра "Зарубежные"
    """
    with allure.step("Step 1: Reopen pages"):
        browser.open()

    with allure.step("Step 2: Present elements selector"):
        filter_selectors: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_SELECTOR)

    with allure.step("Step 3: Open Country selector and find all the elements of the selector"):

        with allure.step("Step 3_1: Open Country selector"):
            browser.click_enable_button(filter_selectors[0])

        with allure.step("Step 3_2: Find all elements of selector"):
            selector_countries_elements: list[WebElement] = browser.get_elements(FilterBlockLocators.SELECTOR_ELEMENT)

    with allure.step("Step 4: To find and activate element of selector СССР"):

        with allure.step("Step 4_1: Find element of selector СССР"):
            selector_element: WebElement = browser.find_element_for_text(
                selector_countries_elements,
                FilterBlockTexts.SELECTOR_ELEMENT_COUNTRIES_TEXT[2]
            )

        with allure.step("Step 4_2: Activate element of selector СССР"):
            browser.click_enable_button(selector_element)

    with allure.step("Step 5: Find filter buttons"):
        filter_disable_buttons: list[WebElement] = browser.get_elements(FilterBlockLocators.FILTER_BUTTON)

    with allure.step("Step 6: To find and press 'Зарубежные' button"):

        with allure.step("Step 6_1: To find 'Зарубежные' button"):
            filter_disable_button: WebElement = browser.find_element_for_text(
                filter_disable_buttons,
                FilterBlockTexts.FILTER_BUTTON_TEXTS[4]
            )

        with allure.step("Step 6_2: Click 'Зарубежные' button"):
            browser.click_disable_button(filter_disable_button)

