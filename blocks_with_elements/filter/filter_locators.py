from selenium.webdriver.common.by import By


class FilterBlockLocators:
    """
    Класс описывающий блок фильтров
    """

    BLOCK_FILTERS = (By.CSS_SELECTOR, ".styles_sticky__pWqAL")
    FILTER_BUTTON = (By.CSS_SELECTOR, ".styles_root__omMgy")
    FILTER_BUTTON_ON = (By.CSS_SELECTOR, ".styles_active__mCHvQ")
    FILTER_BUTTON_DISABLE = (By.CSS_SELECTOR, ".styles_disabled__VYDrH")

    FILTER_SELECTOR = (By.CSS_SELECTOR, ".styles_selectButton__4xHt7")
    SELECTOR_ELEMENT = (By.CSS_SELECTOR, ".styles_item__swkWa")
    HIDE_SELECTOR = (By.CSS_SELECTOR, ".styles_heading__djRMl")
