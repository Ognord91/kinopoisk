import random

from selenium.common import NoSuchElementException, TimeoutException, ElementClickInterceptedException
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    """
    RU: Базовый класс, наследуется другими классами.
    """

    def __init__(self, browser, url: str = "", timeout: int = 10):
        self.timeout = timeout
        self.browser: WebDriver = browser
        self.url: str = url
        self.wait = WebDriverWait(browser, self.timeout)

    def open(self):
        self.browser.get(self.url)

    def click_element(self, locator: tuple | WebElement, clickable: bool = True) -> bool:
        """
        Клик на web-элемент с проверкой на видимость и кликабельность.
        param: clickable: Флаг отвечает - должна ли кнопка нажиматься
        """
        try:
            self.wait.until(
                EC.element_to_be_clickable(
                    self.get_element(*locator)
                    if isinstance(locator, tuple)
                    else locator
                )
            ).click()
        except (NoSuchElementException, TimeoutException):
            return False
        except ElementClickInterceptedException:
            if clickable:
                return False
            else:
                return True

        return clickable

    def get_element(self, locator: tuple[str, str], element_exist: bool = True) -> WebElement | None:
        """
        Найти и вернуть web-элемент, предварительно ожидая его видимости.
        param: element_exist: Флаг отвечает - должен ли элемент существовать
        """
        try:
            element = self.wait.until(EC.visibility_of_element_located(locator))

        except TimeoutException:
            if element_exist:
                raise AssertionError(f"Element, not found {locator}")
            else:
                return None
        return element

    def get_elements(self, locators: tuple[str, str], element_exist: bool = True) -> list[WebElement]:
        """
        Найти и вернуть список web-элементов, предварительно ожидая их видимости.
        param: element_exist: Флаг отвечает - должены ли элементы существовать
        """
        try:
            elements: list = self.wait.until(EC.visibility_of_all_elements_located(locators))

        except TimeoutException:
            if element_exist:
                raise AssertionError(f"Elements, not found {locators}")
            else:
                return []
        else:
            return elements

    def get_random_value_and_index(self, input_list: list) -> tuple[int, str]:
        """
        Функция получает случайны элемент из списка, и возвращает его индекс и значение

        params: input_list (list): Список, из которого нужно выбрать случайное значение.
        returns: Индекс элемента, Значение элемента
        """

        random_index: int = (random.randint(0, len(input_list) - 1))
        random_value: str = input_list[random_index]

        return random_index, random_value


