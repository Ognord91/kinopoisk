from selenium.webdriver.remote.webelement import WebElement

from blocks_with_elements.filter.filter_static_texts import FilterBlockTexts
from pages.base_page import BasePage


class TopPage(BasePage):
    """
    Класс для взаимодействия со страницей топ фильмы
    """

    def check_quantity_elements(self, elements: list[WebElement], quantity_elements: int) -> None:
        """
        Проверяет колличество представленных элементов
        """
        assert len(elements) == quantity_elements, (
            "Quantity elements does not match the expected\n"
            f"Expected quantity elements {quantity_elements}\n"
            f"Actual quantity elements {len(elements)}"
        )

    def check_texts_elements(self, elements: list[WebElement], check_texts: list) -> None:
        """
        Проверяет соответсвие текстов элементов
        """
        missing_texts: list = []

        for element in elements:
            if element.text not in check_texts:
                missing_texts.append(element.text)

        assert not missing_texts, (
            "Elements texts does not match the expected\n"
            f"Following texts is missing {missing_texts}\n"
        )

    def check_text_element(self, element: WebElement, check_text: str) -> None:
        """
        Проверяет соответсвие текста элементa
        """
        assert element.text == check_text, ("Element text does not match the expected\n"
                                            f"Expected text: {check_text}\n"
                                            f"Actual text: {element.text}")

    def filtrate_random_generate_data(self, text_buttons: list) -> tuple[int, str]:
        """
        Отфильтровывает из рандомно сгенерированных данных заблокированные кнопки
        """
        random_index: int
        random_value: str

        random_index, random_value = self.get_random_value_and_index(text_buttons)

        if random_value not in FilterBlockTexts.BUTTON_DISABLE_TEXTS:
            return random_index, random_value
        else:
            return self.filtrate_random_generate_data(text_buttons)

    def click_enable_button(self, locator: tuple | WebElement) -> None:
        """
        Кликает на разблокированную кнопку
        """
        assert self.click_element(locator, clickable=True), f"Couldn't press the button {locator}"

    def click_disable_button(self, locator: tuple | WebElement) -> None:
        """
        Кликает на заблокированную кнопку
        """
        assert self.click_element(locator, clickable=False), f"Possible to click on disable button {locator}"

    def click_enable_buttons(self, locators: list[WebElement]) -> None:
        """
        Проверяет кликабельность нескольких разблокированных кнопок
        """
        errors: list = []

        for locator in locators:
            if not self.click_element(locator, clickable=True):
                errors.append(locator)

        assert not errors, (
            f"Failed to click on the following buttons: {errors}\n"
        )

    def click_disable_buttons(self, locators: list[WebElement]) -> None:
        """
        Проверяет кликабельность нескольких заблокированных кнопок
        """
        errors: list = []

        for locator in locators:
            if not self.click_element(locator, clickable=False):
                errors.append(locator)

        assert not errors, (
            f"Succeeded in clicking on the following disabled buttons {errors}\n"
        )

    def find_element_for_text(self, elements: list[WebElement], element_text: str) -> WebElement:
        """
        Находит элемент по заданному тексту
        """

        for element in elements:
            if element.text == element_text:
                return element

        raise AssertionError("Element with the corresponding text not found.\n"
                             f"Find Text: {element_text}")

